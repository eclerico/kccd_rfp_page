KCCD Drupal RFP System.

This module allows for input and display of RFPs. 

1. Review and istall the dependencies listed in the .info file of this module.

2. Enable the KCCD RFP Page Core module

3. Navigate to the RFP Admin Area: http://your.drupal.site/rfp-admin-area

4. Input a new RFP. the 'Published' setting is off by default - be sure to publish your RFP so that it appears in the admin area.