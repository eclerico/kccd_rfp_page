<?php
/**
 * @file
 * kccd_rfp_page_rfp_content_type.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function kccd_rfp_page_rfp_content_type_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-rfp_listing-field_rfp_addendums'
  $field_instances['node-rfp_listing-field_rfp_addendums'] = array(
    'bundle' => 'rfp_listing',
    'deleted' => 0,
    'description' => 'Attach addendum(s) to this RFP',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_table',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_addendums',
    'label' => 'RFP Addendums',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'kccd_rfp_docs',
      'file_extensions' => 'txt pdf xls doc',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_attachment'
  $field_instances['node-rfp_listing-field_rfp_attachment'] = array(
    'bundle' => 'rfp_listing',
    'deleted' => 0,
    'description' => 'Attach the RFP document to this listing',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_table',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_attachment',
    'label' => 'RFP Attachment',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'kccd_rfp_docs',
      'file_extensions' => 'txt pdf xls doc',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_board_award_date'
  $field_instances['node-rfp_listing-field_rfp_board_award_date'] = array(
    'bundle' => 'rfp_listing',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_board_award_date',
    'label' => 'Board Award Date',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_commencement_date'
  $field_instances['node-rfp_listing-field_rfp_commencement_date'] = array(
    'bundle' => 'rfp_listing',
    'deleted' => 0,
    'description' => 'The date the contract awarded for this RFP will commence. This is the date the RFP listing will disappear from public view on this system. ',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_commencement_date',
    'label' => 'Commencement Date',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_description'
  $field_instances['node-rfp_listing-field_rfp_description'] = array(
    'bundle' => 'rfp_listing',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter a brief description for this RFP listing',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_description',
    'label' => 'RFP Description',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_id'
  $field_instances['node-rfp_listing-field_rfp_id'] = array(
    'bundle' => 'rfp_listing',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The assigned ID referring to this RFP',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_id',
    'label' => 'RFP ID',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 24,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_notice_of_award_date'
  $field_instances['node-rfp_listing-field_rfp_notice_of_award_date'] = array(
    'bundle' => 'rfp_listing',
    'deleted' => 0,
    'description' => 'The date this RFP will be awarded',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_notice_of_award_date',
    'label' => 'Notice of Award Date',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_start_date'
  $field_instances['node-rfp_listing-field_rfp_start_date'] = array(
    'bundle' => 'rfp_listing',
    'deleted' => 0,
    'description' => 'The date that this RFP process will officially begin. This is the date this listing will become visible to the public on this system.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_start_date',
    'label' => 'RFP Start Date',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_status'
  $field_instances['node-rfp_listing-field_rfp_status'] = array(
    'bundle' => 'rfp_listing',
    'default_value' => array(
      0 => array(
        'value' => 'status',
        'format' => 'filtered_html',
      ),
    ),
    'deleted' => 0,
    'description' => 'This field is a placeholder for views. It is not intended to be edited by end users.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_status',
    'label' => 'Status',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 32,
      ),
      'type' => 'text_textfield',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-rfp_listing-field_rfp_submission_deadline'
  $field_instances['node-rfp_listing-field_rfp_submission_deadline'] = array(
    'bundle' => 'rfp_listing',
    'deleted' => 0,
    'description' => 'The date that a vendor must submit their proposal',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_rfp_submission_deadline',
    'label' => 'Submission Deadline',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Attach addendum(s) to this RFP');
  t('Attach the RFP document to this listing');
  t('Board Award Date');
  t('Commencement Date');
  t('Enter a brief description for this RFP listing');
  t('Notice of Award Date');
  t('RFP Addendums');
  t('RFP Attachment');
  t('RFP Description');
  t('RFP ID');
  t('RFP Start Date');
  t('Status');
  t('Submission Deadline');
  t('The assigned ID referring to this RFP');
  t('The date that a vendor must submit their proposal');
  t('The date that this RFP process will officially begin. This is the date this listing will become visible to the public on this system.');
  t('The date the contract awarded for this RFP will commence. This is the date the RFP listing will disappear from public view on this system. ');
  t('The date this RFP will be awarded');
  t('This field is a placeholder for views. It is not intended to be edited by end users.');

  return $field_instances;
}
