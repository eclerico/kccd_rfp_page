<?php
/**
 * @file
 * kccd_rfp_page_rfp_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kccd_rfp_page_rfp_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kccd_rfp_page_rfp_content_type_node_info() {
  $items = array(
    'rfp_listing' => array(
      'name' => t('RFP Listing'),
      'base' => 'node_content',
      'description' => t('List an RFP with associated files, description and significant milestone dates etc.'),
      'has_title' => '1',
      'title_label' => t('RFP Title'),
      'help' => '',
    ),
  );
  return $items;
}
