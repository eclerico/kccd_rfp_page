<?php
/**
 * @file
 * kccd_rfp_page_misc_settings.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function kccd_rfp_page_misc_settings_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer_kccd_rfp_page_settings'.
  $permissions['administer_kccd_rfp_page_settings'] = array(
    'name' => 'administer_kccd_rfp_page_settings',
    'roles' => array(
      'kccd rfp page admin' => 'kccd rfp page admin',
    ),
    'module' => 'kccd_rfp_page',
  );

  // Exported permission: 'create rfp_listing content'.
  $permissions['create rfp_listing content'] = array(
    'name' => 'create rfp_listing content',
    'roles' => array(
      'administrator' => 'administrator',
      'kccd rfp page admin' => 'kccd rfp page admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any rfp_listing content'.
  $permissions['delete any rfp_listing content'] = array(
    'name' => 'delete any rfp_listing content',
    'roles' => array(
      'administrator' => 'administrator',
      'kccd rfp page admin' => 'kccd rfp page admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any rfp_listing content'.
  $permissions['edit any rfp_listing content'] = array(
    'name' => 'edit any rfp_listing content',
    'roles' => array(
      'administrator' => 'administrator',
      'kccd rfp page admin' => 'kccd rfp page admin',
    ),
    'module' => 'node',
  );

  return $permissions;
}
