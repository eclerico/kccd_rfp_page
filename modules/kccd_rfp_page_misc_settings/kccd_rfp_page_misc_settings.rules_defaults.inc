<?php
/**
 * @file
 * kccd_rfp_page_misc_settings.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function kccd_rfp_page_misc_settings_default_rules_configuration() {
  $items = array();
  $items['rules_kccd_forward_after_create_rfp'] = entity_import('rules_config', '{ "rules_kccd_forward_after_create_rfp" : {
      "LABEL" : "KCCD Forward After Create RFP",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "KCCD" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--rfp_listing" : { "bundle" : "rfp_listing" } },
      "DO" : [ { "redirect" : { "url" : "rfp-admin-area" } } ]
    }
  }');
  return $items;
}
