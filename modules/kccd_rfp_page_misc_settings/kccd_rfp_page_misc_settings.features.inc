<?php
/**
 * @file
 * kccd_rfp_page_misc_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kccd_rfp_page_misc_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
