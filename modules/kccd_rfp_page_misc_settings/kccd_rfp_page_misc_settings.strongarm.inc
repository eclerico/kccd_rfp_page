<?php
/**
 * @file
 * kccd_rfp_page_misc_settings.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function kccd_rfp_page_misc_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_brief';
  $strongarm->value = 'm/d/Y';
  $export['date_format_brief'] = $strongarm;

  return $export;
}
