<?php
/**
 * @file
 * kccd_rfp_page_misc_settings.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function kccd_rfp_page_misc_settings_user_default_roles() {
  $roles = array();

  // Exported role: kccd rfp page admin.
  $roles['kccd rfp page admin'] = array(
    'name' => 'kccd rfp page admin',
    'weight' => 3,
  );

  return $roles;
}
