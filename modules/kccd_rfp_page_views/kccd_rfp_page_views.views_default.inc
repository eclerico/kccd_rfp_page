<?php
/**
 * @file
 * kccd_rfp_page_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function kccd_rfp_page_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'kccd_current_rfp_listing';
  $view->description = 'The publicly available page that lists current RFPs with status and available attachments.';
  $view->tag = 'KCCD RFP Page';
  $view->base_table = 'node';
  $view->human_name = 'Current RFP Listing';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Current RFP Listing';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: RFP Attachment */
  $handler->display->display_options['fields']['field_rfp_attachment']['id'] = 'field_rfp_attachment';
  $handler->display->display_options['fields']['field_rfp_attachment']['table'] = 'field_data_field_rfp_attachment';
  $handler->display->display_options['fields']['field_rfp_attachment']['field'] = 'field_rfp_attachment';
  $handler->display->display_options['fields']['field_rfp_attachment']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_attachment']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_rfp_attachment']['type'] = 'file_url_plain';
  /* Field: Content: RFP Addendums */
  $handler->display->display_options['fields']['field_rfp_addendums']['id'] = 'field_rfp_addendums';
  $handler->display->display_options['fields']['field_rfp_addendums']['table'] = 'field_data_field_rfp_addendums';
  $handler->display->display_options['fields']['field_rfp_addendums']['field'] = 'field_rfp_addendums';
  $handler->display->display_options['fields']['field_rfp_addendums']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_addendums']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_rfp_addendums']['type'] = 'custom_formatters_kccd_addendum_file';
  $handler->display->display_options['fields']['field_rfp_addendums']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_rfp_addendums']['multi_type'] = 'ul';
  /* Field: Content: RFP Description */
  $handler->display->display_options['fields']['field_rfp_description']['id'] = 'field_rfp_description';
  $handler->display->display_options['fields']['field_rfp_description']['table'] = 'field_data_field_rfp_description';
  $handler->display->display_options['fields']['field_rfp_description']['field'] = 'field_rfp_description';
  $handler->display->display_options['fields']['field_rfp_description']['exclude'] = TRUE;
  /* Field: Content: RFP ID */
  $handler->display->display_options['fields']['field_rfp_id']['id'] = 'field_rfp_id';
  $handler->display->display_options['fields']['field_rfp_id']['table'] = 'field_data_field_rfp_id';
  $handler->display->display_options['fields']['field_rfp_id']['field'] = 'field_rfp_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<a href="[field_rfp_attachment]">[title]</a> <br />
[field_rfp_description] <br />
[field_rfp_addendums]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: RFP Start Date */
  $handler->display->display_options['fields']['field_rfp_start_date']['id'] = 'field_rfp_start_date';
  $handler->display->display_options['fields']['field_rfp_start_date']['table'] = 'field_data_field_rfp_start_date';
  $handler->display->display_options['fields']['field_rfp_start_date']['field'] = 'field_rfp_start_date';
  $handler->display->display_options['fields']['field_rfp_start_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_start_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Submission Deadline */
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['id'] = 'field_rfp_submission_deadline';
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['table'] = 'field_data_field_rfp_submission_deadline';
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['field'] = 'field_rfp_submission_deadline';
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Notice of Award Date */
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['id'] = 'field_rfp_notice_of_award_date';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['table'] = 'field_data_field_rfp_notice_of_award_date';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['field'] = 'field_rfp_notice_of_award_date';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Board Award Date */
  $handler->display->display_options['fields']['field_rfp_board_award_date']['id'] = 'field_rfp_board_award_date';
  $handler->display->display_options['fields']['field_rfp_board_award_date']['table'] = 'field_data_field_rfp_board_award_date';
  $handler->display->display_options['fields']['field_rfp_board_award_date']['field'] = 'field_rfp_board_award_date';
  $handler->display->display_options['fields']['field_rfp_board_award_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_board_award_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Commencement Date */
  $handler->display->display_options['fields']['field_rfp_commencement_date']['id'] = 'field_rfp_commencement_date';
  $handler->display->display_options['fields']['field_rfp_commencement_date']['table'] = 'field_data_field_rfp_commencement_date';
  $handler->display->display_options['fields']['field_rfp_commencement_date']['field'] = 'field_rfp_commencement_date';
  $handler->display->display_options['fields']['field_rfp_commencement_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_commencement_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_rfp_status']['id'] = 'field_rfp_status';
  $handler->display->display_options['fields']['field_rfp_status']['table'] = 'field_data_field_rfp_status';
  $handler->display->display_options['fields']['field_rfp_status']['field'] = 'field_rfp_status';
  $handler->display->display_options['fields']['field_rfp_status']['type'] = 'custom_formatters_kccd_rfp_status';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'rfp_listing' => 'rfp_listing',
  );
  /* Filter criterion: Content: Commencement Date (field_rfp_commencement_date) */
  $handler->display->display_options['filters']['field_rfp_commencement_date_value']['id'] = 'field_rfp_commencement_date_value';
  $handler->display->display_options['filters']['field_rfp_commencement_date_value']['table'] = 'field_data_field_rfp_commencement_date';
  $handler->display->display_options['filters']['field_rfp_commencement_date_value']['field'] = 'field_rfp_commencement_date_value';
  $handler->display->display_options['filters']['field_rfp_commencement_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_rfp_commencement_date_value']['granularity'] = 'minute';
  $handler->display->display_options['filters']['field_rfp_commencement_date_value']['default_date'] = 'now';
  $handler->display->display_options['filters']['field_rfp_commencement_date_value']['year_range'] = '-0:+0';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'current-rfp-listing';
  $export['kccd_current_rfp_listing'] = $view;

  $view = new view();
  $view->name = 'rfp_admin_area';
  $view->description = 'CRUD for RFP Pages';
  $view->tag = 'KCCD RFP Page';
  $view->base_table = 'node';
  $view->human_name = 'RFP Admin Area';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'RFP Admin Area';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer kccd_rfp_page settings';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<a href="node/add/rfp-listing" class="button">Add new RFP listing</a>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: RFP ID */
  $handler->display->display_options['fields']['field_rfp_id']['id'] = 'field_rfp_id';
  $handler->display->display_options['fields']['field_rfp_id']['table'] = 'field_data_field_rfp_id';
  $handler->display->display_options['fields']['field_rfp_id']['field'] = 'field_rfp_id';
  $handler->display->display_options['fields']['field_rfp_id']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_id']['alter']['text'] = '<h2>[field_rfp_id]</h2>
<a href="/node/[nid]/edit" class="button">Edit</a>';
  /* Field: Content: RFP Description */
  $handler->display->display_options['fields']['field_rfp_description']['id'] = 'field_rfp_description';
  $handler->display->display_options['fields']['field_rfp_description']['table'] = 'field_data_field_rfp_description';
  $handler->display->display_options['fields']['field_rfp_description']['field'] = 'field_rfp_description';
  $handler->display->display_options['fields']['field_rfp_description']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'RFP Title / Description';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h2>[title]</h2>
[field_rfp_description]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Submission Deadline */
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['id'] = 'field_rfp_submission_deadline';
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['table'] = 'field_data_field_rfp_submission_deadline';
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['field'] = 'field_rfp_submission_deadline';
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_submission_deadline']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: RFP Start Date */
  $handler->display->display_options['fields']['field_rfp_start_date']['id'] = 'field_rfp_start_date';
  $handler->display->display_options['fields']['field_rfp_start_date']['table'] = 'field_data_field_rfp_start_date';
  $handler->display->display_options['fields']['field_rfp_start_date']['field'] = 'field_rfp_start_date';
  $handler->display->display_options['fields']['field_rfp_start_date']['label'] = 'Start Date / Submission Deadline';
  $handler->display->display_options['fields']['field_rfp_start_date']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_start_date']['alter']['text'] = '[field_rfp_start_date] /<br />
[field_rfp_submission_deadline] ';
  $handler->display->display_options['fields']['field_rfp_start_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Board Award Date */
  $handler->display->display_options['fields']['field_rfp_board_award_date']['id'] = 'field_rfp_board_award_date';
  $handler->display->display_options['fields']['field_rfp_board_award_date']['table'] = 'field_data_field_rfp_board_award_date';
  $handler->display->display_options['fields']['field_rfp_board_award_date']['field'] = 'field_rfp_board_award_date';
  $handler->display->display_options['fields']['field_rfp_board_award_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_board_award_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Notice of Award Date */
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['id'] = 'field_rfp_notice_of_award_date';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['table'] = 'field_data_field_rfp_notice_of_award_date';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['field'] = 'field_rfp_notice_of_award_date';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['label'] = 'Notice of Award / Board Award';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['alter']['text'] = '[field_rfp_notice_of_award_date] /<br />
[field_rfp_board_award_date]';
  $handler->display->display_options['fields']['field_rfp_notice_of_award_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Commencement Date */
  $handler->display->display_options['fields']['field_rfp_commencement_date']['id'] = 'field_rfp_commencement_date';
  $handler->display->display_options['fields']['field_rfp_commencement_date']['table'] = 'field_data_field_rfp_commencement_date';
  $handler->display->display_options['fields']['field_rfp_commencement_date']['field'] = 'field_rfp_commencement_date';
  $handler->display->display_options['fields']['field_rfp_commencement_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: RFP Attachment */
  $handler->display->display_options['fields']['field_rfp_attachment']['id'] = 'field_rfp_attachment';
  $handler->display->display_options['fields']['field_rfp_attachment']['table'] = 'field_data_field_rfp_attachment';
  $handler->display->display_options['fields']['field_rfp_attachment']['field'] = 'field_rfp_attachment';
  $handler->display->display_options['fields']['field_rfp_attachment']['click_sort_column'] = 'fid';
  /* Field: Content: RFP Addendums */
  $handler->display->display_options['fields']['field_rfp_addendums']['id'] = 'field_rfp_addendums';
  $handler->display->display_options['fields']['field_rfp_addendums']['table'] = 'field_data_field_rfp_addendums';
  $handler->display->display_options['fields']['field_rfp_addendums']['field'] = 'field_rfp_addendums';
  $handler->display->display_options['fields']['field_rfp_addendums']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_rfp_addendums']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_rfp_addendums']['multi_type'] = 'ul';
  /* Field: Content: Published */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'node';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Post date';
  /* Sort criterion: Content: RFP Start Date (field_rfp_start_date) */
  $handler->display->display_options['sorts']['field_rfp_start_date_value']['id'] = 'field_rfp_start_date_value';
  $handler->display->display_options['sorts']['field_rfp_start_date_value']['table'] = 'field_data_field_rfp_start_date';
  $handler->display->display_options['sorts']['field_rfp_start_date_value']['field'] = 'field_rfp_start_date_value';
  $handler->display->display_options['sorts']['field_rfp_start_date_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_rfp_start_date_value']['expose']['label'] = 'Start Date';
  /* Sort criterion: Content: Board Award Date (field_rfp_board_award_date) */
  $handler->display->display_options['sorts']['field_rfp_board_award_date_value']['id'] = 'field_rfp_board_award_date_value';
  $handler->display->display_options['sorts']['field_rfp_board_award_date_value']['table'] = 'field_data_field_rfp_board_award_date';
  $handler->display->display_options['sorts']['field_rfp_board_award_date_value']['field'] = 'field_rfp_board_award_date_value';
  $handler->display->display_options['sorts']['field_rfp_board_award_date_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_rfp_board_award_date_value']['expose']['label'] = 'Board Award Date';
  /* Sort criterion: Content: Commencement Date (field_rfp_commencement_date) */
  $handler->display->display_options['sorts']['field_rfp_commencement_date_value']['id'] = 'field_rfp_commencement_date_value';
  $handler->display->display_options['sorts']['field_rfp_commencement_date_value']['table'] = 'field_data_field_rfp_commencement_date';
  $handler->display->display_options['sorts']['field_rfp_commencement_date_value']['field'] = 'field_rfp_commencement_date_value';
  $handler->display->display_options['sorts']['field_rfp_commencement_date_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_rfp_commencement_date_value']['expose']['label'] = 'Commencement Date';
  /* Sort criterion: Content: Notice of Award Date (field_rfp_notice_of_award_date) */
  $handler->display->display_options['sorts']['field_rfp_notice_of_award_date_value']['id'] = 'field_rfp_notice_of_award_date_value';
  $handler->display->display_options['sorts']['field_rfp_notice_of_award_date_value']['table'] = 'field_data_field_rfp_notice_of_award_date';
  $handler->display->display_options['sorts']['field_rfp_notice_of_award_date_value']['field'] = 'field_rfp_notice_of_award_date_value';
  $handler->display->display_options['sorts']['field_rfp_notice_of_award_date_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_rfp_notice_of_award_date_value']['expose']['label'] = 'Notice of Award Date';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'rfp_listing' => 'rfp_listing',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'rfp-admin-area';
  $export['rfp_admin_area'] = $view;

  return $export;
}
