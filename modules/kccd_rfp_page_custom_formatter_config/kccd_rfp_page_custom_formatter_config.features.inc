<?php
/**
 * @file
 * kccd_rfp_page_custom_formatter_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kccd_rfp_page_custom_formatter_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
}
