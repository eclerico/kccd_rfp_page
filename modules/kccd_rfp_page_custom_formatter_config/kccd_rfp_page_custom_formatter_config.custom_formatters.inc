<?php
/**
 * @file
 * kccd_rfp_page_custom_formatter_config.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function kccd_rfp_page_custom_formatter_config_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'kccd_addendum_file';
  $formatter->label = 'KCCD Addendum File';
  $formatter->description = 'Provide a custom formatter that lists addendum files as \'01/01/1961 Addendum\'';
  $formatter->mode = 'php';
  $formatter->field_types = 'file';
  $formatter->code = '$render_array = array(
		\'#prefix\' => \'<div>\',
		\'#suffix\' => \'</div>\',
		);
foreach ($variables[\'#items\'] as $item) {
	$render_array[] = array(
		\'#prefix\' => \'<div class="kccd-rfp-page-addendum-file-list">\',
		\'#suffix\' => \'</div>\',
		0 => array(
			\'#type\' => \'markup\',
			\'#markup\' => date(\'m/d/Y\', $item[\'timestamp\']) . \' \',
		),
		1 => array(
			\'#theme\' => \'link\',
			\'#text\' => t(\'Addendum\'),
			\'#path\' => file_create_url($item[\'uri\']),
			\'#options\' => array(\'attributes\' => array(),),
		),
	);
}
return $render_array;';
  $formatter->fapi = '';
  $export['kccd_addendum_file'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'kccd_rfp_status';
  $formatter->label = 'KCCD RFP Status';
  $formatter->description = 'Display the current status of an RFP listing based upon the relationship of current time and various milestone dates.';
  $formatter->mode = 'php';
  $formatter->field_types = 'text';
  $formatter->code = '$lang = $variables[\'#object\']->language;
$start = strtotime($variables[\'#object\']->field_rfp_start_date[$lang][0][\'value\']);
$submit_deadline = strtotime($variables[\'#object\']->field_rfp_submission_deadline[$lang][0][\'value\']);
$award_date_notice = strtotime($variables[\'#object\']->field_rfp_notice_of_award_date[$lang][0][\'value\']);
$award_date = strtotime($variables[\'#object\']->field_rfp_board_award_date[$lang][0][\'value\']);
$commence_date = strtotime($variables[\'#object\']->field_rfp_commencement_date[$lang][0][\'value\']);
$now = time();
$return = \'hello kitty\';
switch (true) {
  case ($now >= $start && $now <= $submit_deadline):
    $return = t(\'%status\', array (\'%status\' => variable_get(\'kccd_rfp_submit_period_string\',\'\')));
    break;
  case ($now > $submit_deadline && $now <= $award_date_notice):
    $return = t(\'%status\', array (\'%status\' => variable_get(\'kccd_rfp_review_period_string\',\'\')));
    break;
  case ($now > $award_date_notice && $now <= $award_date):
    $return = t(\'%status\', array (\'%status\' => variable_get(\'kccd_rfp_selected_period_string\',\'\')));
    break;
  case ($now > $award_date && $now <= $commence_date):
    $return = t(\'%status\', array (\'%status\' => variable_get(\'kccd_rfp_awarded_period_string\',\'\')));
    break;
}

$render_array = array(
  \'#type\' => \'markup\',
  \'#markup\' => $return,
  \'#prefix\' => \'<div class="kccd-rfp-page-status">\',
  \'#suffix\' => \'</div>\'
  );

return array($render_array);';
  $formatter->fapi = '';
  $export['kccd_rfp_status'] = $formatter;

  return $export;
}
